package bookstore;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;

public class Logs extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Logs frame = new Logs();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Logs() {
		setTitle("Logs Info");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(175, 238, 238));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblId = new JLabel("ID");
		lblId.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblId.setBounds(90, 14, 46, 14);
		contentPane.add(lblId);
		
		JLabel lblDetails = new JLabel("Details");
		lblDetails.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblDetails.setBounds(90, 39, 46, 14);
		contentPane.add(lblDetails);
		
		JLabel lblCreatedDatetime = new JLabel("Created Date/Time");
		lblCreatedDatetime.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblCreatedDatetime.setBounds(90, 64, 120, 14);
		contentPane.add(lblCreatedDatetime);
		
		JLabel lblModifiedBy = new JLabel("Modified By");
		lblModifiedBy.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblModifiedBy.setBounds(90, 92, 100, 14);
		contentPane.add(lblModifiedBy);
		
		textField = new JTextField();
		textField.setBounds(220, 11, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(220, 36, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(220, 61, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(220, 89, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnOk = new JButton("OK");
		btnOk.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		btnOk.setBounds(47, 138, 89, 23);
		contentPane.add(btnOk);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		btnCancel.setBounds(261, 138, 89, 23);
		contentPane.add(btnCancel);
	}
}
