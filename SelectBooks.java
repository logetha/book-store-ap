package bookstore;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bookstore.book.collection.Black_Beauty;
import bookstore.book.collection.Brand_Famous;
import bookstore.book.collection.Dying_To_Famous;
import bookstore.book.collection.Fairy_Tales;
import bookstore.book.collection.Harry_Porter;
import bookstore.book.collection.Jurasic_Park;
import bookstore.book.collection.Pirate_Tale;
import bookstore.book.collection.Pokemon;
import bookstore.book.collection.Robin_Hood;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class SelectBooks extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField textField;
	static int bookId;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SelectBooks frame = new SelectBooks();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SelectBooks() {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 450, 410);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		String data[][] = null;
		String column[] = null;
		try {
			Connection con = DB.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from books", ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			ResultSet rs = ps.executeQuery();

			ResultSetMetaData rsmd = rs.getMetaData();
			int cols = rsmd.getColumnCount();
			column = new String[cols];
			for (int i = 1; i <= cols; i++) {
				column[i - 1] = rsmd.getColumnName(i);
			}

			rs.last();
			int rows = rs.getRow();
			rs.beforeFirst();

			data = new String[rows][cols];
			int count = 0;
			while (rs.next()) {
				for (int i = 1; i <= cols; i++) {
					data[count][i - 1] = rs.getString(i);
				}
				count++;
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		contentPane.setLayout(null);

		table = new JTable(data, column);
		JScrollPane sp = new JScrollPane(table);
		sp.setBounds(5, 5, 424, 251);

		contentPane.add(sp);

		JButton btnNewButton = new JButton("New button");
		btnNewButton.setBounds(15, 277, 89, 23);
		contentPane.add(btnNewButton);

		JButton btnViewBook = new JButton("View Book");
		btnViewBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				bookId = Integer.parseInt(textField.getText());
				if (bookId > 0) {
					if (AddBookDB.checkBook(bookId)) {
						SelectBookView.main(new String[] {});
						// frame.dispose();

					} else {
						JOptionPane.showMessageDialog(SelectBooks.this, "Sorry, unable to issue!");
					} // end of save if-else

				} else {
					JOptionPane.showMessageDialog(SelectBooks.this, "Sorry, Callno doesn't exist!");
				} // end of checkbook if-else
			}
		});
		btnViewBook.setBounds(323, 277, 89, 23);
		contentPane.add(btnViewBook);

		textField = new JTextField();
		textField.setBounds(239, 278, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
	}
}
